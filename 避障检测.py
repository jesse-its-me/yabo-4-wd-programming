import RPi.GPIO as GPIO
import time

#小车电机引脚定义
IN1 = 20
IN2 = 21
IN3 = 19
IN4 = 26
ENA = 16
ENB = 13

#设置GPIO口为BCM编码方式
GPIO.setmode(GPIO.BCM)

#忽略警告信息
GPIO.setwarnings(False)

#电机引脚初始化操作
def motor_init():
    global pwm_ENA
    global pwm_ENB
    global delaytime
    GPIO.setup(ENA,GPIO.OUT,initial=GPIO.HIGH)
    GPIO.setup(IN1,GPIO.OUT,initial=GPIO.LOW)
    GPIO.setup(IN2,GPIO.OUT,initial=GPIO.LOW)
    GPIO.setup(ENB,GPIO.OUT,initial=GPIO.HIGH)
    GPIO.setup(IN3,GPIO.OUT,initial=GPIO.LOW)
    GPIO.setup(IN4,GPIO.OUT,initial=GPIO.LOW)
    #设置pwm引脚和频率为2000hz
    pwm_ENA = GPIO.PWM(ENA, 2000)
    pwm_ENB = GPIO.PWM(ENB, 2000)
    pwm_ENA.start(0)
    pwm_ENB.start(0)

#小车前进	
def run(delaytime):
    GPIO.output(IN1, GPIO.HIGH)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.HIGH)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车后退
def back(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.HIGH)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.HIGH)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车左转	
def left(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.HIGH)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车右转
def right(delaytime):
    GPIO.output(IN1, GPIO.HIGH)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(10)
    pwm_ENB.ChangeDutyCycle(10)
    time.sleep(delaytime)

#小车原地左转
def spin_left(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.HIGH)
    GPIO.output(IN3, GPIO.HIGH)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(80)
    pwm_ENB.ChangeDutyCycle(80)
    time.sleep(delaytime)

#小车原地右转
def spin_right(delaytime):
    GPIO.output(IN1, GPIO.HIGH)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.HIGH)
    pwm_ENA.ChangeDutyCycle(80)
    pwm_ENB.ChangeDutyCycle(80)
    time.sleep(delaytime)

#小车停止	
def brake(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(80)
    pwm_ENB.ChangeDutyCycle(80)
    time.sleep(delaytime)



class CarUltrasound(object):
    def __init__(self):

        
        self.GPIO_TRIGGER = 1  # GPIO setting (BCM coding)
        self.GPIO_ECHO = 0

        GPIO.setup(self.GPIO_TRIGGER, GPIO.OUT)  # GPIO input/output definiation
        GPIO.setup(self.GPIO_ECHO, GPIO.IN)

        self.dist_mov_ave = 0

    def distMeasure(self):  # distance measuing

        GPIO.output(self.GPIO_TRIGGER, GPIO.HIGH)
        time.sleep(0.000015)
        GPIO.output(self.GPIO_TRIGGER, GPIO.LOW)
        while not GPIO.input(self.GPIO_ECHO):
            pass
        t1 = time.time()
        while GPIO.input(self.GPIO_ECHO):
            pass
        t2 = time.time()
        print
        "distance is %d " % (((t2 - t1) * 340 / 2) * 100)
        time.sleep(0.01)
        return ((t2 - t1) * 340 / 2) * 100

        # GPIO.output(self.GPIO_TRIGGER, False)
        # time.sleep(0.000002)
        # GPIO.output(self.GPIO_TRIGGER, True)  # emit ultrasonic pulse
        # time.sleep(0.00001)                   # last 10us
        # GPIO.output(self.GPIO_TRIGGER, False) # end the pulse

        # ii = 0
        # while GPIO.input(self.GPIO_ECHO) == 0:  # when receiving the echo, ECHO will become 1
        #     ii = ii + 1
        #     if ii > 10000:
        #         print('Ultrasound error: the sensor missed the echo')
        #         return 0
        #     pass
        # start_time = time.time()
        #
        # while GPIO.input(self.GPIO_ECHO) == 1:  # the duration of high level of ECHO is the time between the emitting the pulse and receiving the echo
        #         pass
        # stop_time = time.time()
        #
        # time_elapsed = stop_time - start_time
        # distance = (time_elapsed * 34300) / 2
        #
        # return distance

    def distMeasureMovingAverage(self):
        dist_current = self.distMeasure()
        if dist_current == 0:  # if the sensor missed the echo, the output dis_mov_ave will equal the last dis_mov_ave
            return self.dist_mov_ave
        else:
            self.dist_mov_ave = 0.8 * dist_current + 0.2 * self.dist_mov_ave  # using the moving average of distance measured by sensor to reduce the error
            return self.dist_mov_ave


if __name__ == '__main__':
    try:
        motor_init()
        car = CarUltrasound()
        while True:
            dist = car.distMeasure()
            #avoid
            print("Measured Distance = {:.2f} cm".format(dist))
            if(dist<=40):
                print("快撞了！！！")
                brake(0.3)     #小车停止2s
                back(0.3)
#                 spin_right(1)#小车右旋5s
                left(0.6)
                
            else:
                print("问题不大。。。")
                run(0.3)      #小车前进5s
#             time.sleep(0.1)

        # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()