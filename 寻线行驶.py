import RPi.GPIO as GPIO
import time

#小车电机引脚定义
IN1 = 20
IN2 = 21
IN3 = 19
IN4 = 26
ENA = 16
ENB = 13

#设置GPIO口为BCM编码方式
GPIO.setmode(GPIO.BCM)

#忽略警告信息
GPIO.setwarnings(False)

#电机引脚初始化操作
def motor_init():
    global pwm_ENA
    global pwm_ENB
    global delaytime
    GPIO.setup(ENA,GPIO.OUT,initial=GPIO.HIGH)
    GPIO.setup(IN1,GPIO.OUT,initial=GPIO.LOW)
    GPIO.setup(IN2,GPIO.OUT,initial=GPIO.LOW)
    GPIO.setup(ENB,GPIO.OUT,initial=GPIO.HIGH)
    GPIO.setup(IN3,GPIO.OUT,initial=GPIO.LOW)
    GPIO.setup(IN4,GPIO.OUT,initial=GPIO.LOW)
    #设置pwm引脚和频率为2000hz
    pwm_ENA = GPIO.PWM(ENA, 2000)
    pwm_ENB = GPIO.PWM(ENB, 2000)
    pwm_ENA.start(0)
    pwm_ENB.start(0)

#小车前进	
def run(delaytime):
    GPIO.output(IN1, GPIO.HIGH)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.HIGH)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车后退
def back(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.HIGH)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.HIGH)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车左转	
def left(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.HIGH)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车右转
def right(delaytime):
    GPIO.output(IN1, GPIO.HIGH)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(10)
    pwm_ENB.ChangeDutyCycle(10)
    time.sleep(delaytime)

#小车原地左转
def spin_left(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.HIGH)
    GPIO.output(IN3, GPIO.HIGH)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(80)
    pwm_ENB.ChangeDutyCycle(80)
    time.sleep(delaytime)

#小车原地右转
def spin_right(delaytime):
    GPIO.output(IN1, GPIO.HIGH)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.HIGH)
    pwm_ENA.ChangeDutyCycle(80)
    pwm_ENB.ChangeDutyCycle(80)
    time.sleep(delaytime)

#小车停止	
def brake(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(80)
    pwm_ENB.ChangeDutyCycle(80)
    time.sleep(delaytime)

import numpy as np
import cv2
import math


class LaneDetect (object): #8-59

    def __init__ (self):
        self .VideoReturn = True
        self.num_lane_point = 4
        self.turn_right_speed = 50
        self.turn_left_speed = 50
        self.forward_speed = 40
        self.speed_high = 60
        self.speed_low = 0
        self.cap = cv2.VideoCapture(0)
        self.cap.set(3, 320)
        self.cap.set(4, 240)

    def stop(self):

        cv2.destroyAllWindows( )

    def directionDetect(self, imageFrame):
        ForB='Forward'
        LorR='Brake'
#         print("21")

        #转化为灰度图
        gray=cv2.cvtColor(imageFrame, cv2.COLOR_RGB2GRAY)
        
#         print("22")

        #大律法二值化
        retval, dst=cv2.threshold(gray,0,255,cv2.THRESH_OTSU)
#         print("23")

        height,width=dst.shape
        half_width=int(width/2)
#         print("24")

        right_line_pos=np.zeros((self.num_lane_point,1))
        left_line_pos = np.zeros((self.num_lane_point, 1))
#         print("25")

        img_out=cv2.cvtColor(dst,cv2.COLOR_GRAY2RGB)
#         print("26")
        for i in range(self.num_lane_point):
            detect_height=height-25*(i+1)
            detect_area_left=dst[detect_height,
                             0:half_width-1]
            detect_area_right=dst[detect_height,half_width:width-1]
            line_left = np.where(detect_area_left==0)
            line_right = np.where(detect_area_right == 0)

            if len(line_left[0]):
                left_line_pos[i]=int(np.max(line_left))
            else:
                left_line_pos[i]=0

            if len(line_right[0]):
                right_line_pos[i]=int(np.max(line_right))
            else:
                right_line_pos[i]=half_width-1

            if left_line_pos[i]!=0:
                img_out=cv2.circle(img_out,(left_line_pos[i],detect_height),4,(0,190,255),thickness=10)
            if right_line_pos[i]!=half_width-1:
                img_out =cv2.circle(img_out,(half_width+right_line_pos[i],detect_height),4,(0,0,255),
                                    thickness=10)

        #if self.VideoReturn:

            #cv2.imshow("frame",img_out)
            #cv2.imwrite("f:\\"+imageFile+"-out-2.jpg",img_out)


        left_max=np.max(left_line_pos)
        right_min =np.min(right_line_pos)


        if left_max==0 and right_min==half_width-1:
            pass
        elif left_max==0:
            if right_min>half_width-100:
                ForB='Forward'
                LorR='Brake'
            elif right_min<100:
                ForB='Brake'
                LorR='Left'
            else:
                ForB='Forward'
                LorR='Left'
        elif right_min == half_width-1:
            if left_max<100:
                ForB='Forward'
                LorR='Brake'
            elif left_max>half_width-100:
                ForB='Brake'
                LorR='Right'
            else:
                ForB='Forward'
                LorR='Right'
        else:


            left_line_pos=[ele for ele in left_line_pos if ele !=0]
            right_line_pos=[ele for ele in right_line_pos if ele !=0]
            line_pos=right_line_pos
            if len(left_line_pos)>len(right_line_pos):
                line_pos=left_line_pos

            maxIndex=len(line_pos)-1

            if((line_pos[maxIndex]-line_pos[0])<-20):
                ForB='Forward'
                LorR='left'
            elif((line_pos[0]-line_pos[maxIndex])>20):
                ForB='Forward'
                LorR='Right'
            elif(line_pos[maxIndex]==line_pos[0]):
                ForB='Forward'
                LorR='Brake'

         
        carDirection=''
        if ForB is 'Brake':
            if LorR is 'Left':
                carDirection='left'
            elif LorR is 'right':
                carDirection='right'
            elif LorR is 'brake':
                carDirection='brake'
        elif ForB is 'Forward':
            if LorR is 'left':
                 carDirection="forward left"
            elif LorR is 'Right':#150-167
                 carDirection="forward right"
            elif LorR is 'Brake':
                 carDirection="forward"
        elif ForB is 'Backward':
            if LorR is 'Left':
                carDirection="left"
            elif LorR is 'right':
                carDirection="right"
            elif LorR is 'Brake':
                carDirection = "brake"


        return carDirection




import time

if __name__=='__main__': #170
    try:
        
        motor_init()
#         print("1")
        car=LaneDetect()
#         print("1")
        #imageFile="1"
        while True:
            time.sleep(1)
#             frame_origin=cv2.imread('./predict/2.png')
#             print("1")
            ret,frame_origin = car.cap.read()
#             print("1")
            sp=frame_origin.shape
            print(sp)
            sz1=sp[1]
            sz2=sp[0]
            print(sz1)
            print(sz2)
            cropImag=frame_origin[30:(sz1-150),0:sz2]
#             print("1")
            cv2.imwrite("./predict/out2.png",cropImag)
#             print("1")
            carDirection = car.directionDetect(cropImag)
#             print("1")
            run(10)
            #print(carDirection)
            if carDirection == "forward" :
                run(1)
            elif carDirection == "left" :
                left(1)
            elif carDirection == "right" :
                right(1)        
            print(carDirection)

    except KeyboardInterrupt:
        print("Measurement stupped by User")


    except Exception:
        print("stop")