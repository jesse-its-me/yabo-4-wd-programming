import RPi.GPIO as GPIO
import time

# 第三步 模型应用，识别交通标志图片
# 加载工程中必要的库
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import argparse
import imutils
import cv2


#小车电机引脚定义
IN1 = 20
IN2 = 21
IN3 = 19
IN4 = 26
ENA = 16
ENB = 13

#设置GPIO口为BCM编码方式
GPIO.setmode(GPIO.BCM)

#忽略警告信息
GPIO.setwarnings(False)


#电机引脚初始化操作
def motor_init():
    global pwm_ENA
    global pwm_ENB
    global delaytime
    GPIO.setup(ENA,GPIO.OUT,initial=GPIO.HIGH)
    GPIO.setup(IN1,GPIO.OUT,initial=GPIO.LOW)
    GPIO.setup(IN2,GPIO.OUT,initial=GPIO.LOW)
    GPIO.setup(ENB,GPIO.OUT,initial=GPIO.HIGH)
    GPIO.setup(IN3,GPIO.OUT,initial=GPIO.LOW)
    GPIO.setup(IN4,GPIO.OUT,initial=GPIO.LOW)
    #设置pwm引脚和频率为2000hz
    pwm_ENA = GPIO.PWM(ENA, 1000)
    pwm_ENB = GPIO.PWM(ENB, 1000)
    pwm_ENA.start(0)
    pwm_ENB.start(0)


#小车前进	
def run(delaytime):
    GPIO.output(IN1, GPIO.HIGH)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.HIGH)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车后退
def back(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.HIGH)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.HIGH)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车左转	
def left(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.HIGH)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车右转
def right(delaytime):
    GPIO.output(IN1, GPIO.HIGH)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车原地左转
def spin_left(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.HIGH)
    GPIO.output(IN3, GPIO.HIGH)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车原地右转
def spin_right(delaytime):
    GPIO.output(IN1, GPIO.HIGH)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.HIGH)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)

#小车停止	
def brake(delaytime):
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.LOW)
    GPIO.output(IN3, GPIO.LOW)
    GPIO.output(IN4, GPIO.LOW)
    pwm_ENA.ChangeDutyCycle(20)
    pwm_ENB.ChangeDutyCycle(20)
    time.sleep(delaytime)


class SighRecongination(object):
    def __init__(self):
        # 根据使用的模型，确定图像需要resize的尺寸
        self.norm_size = 64
        self.cap = cv2.VideoCapture(0)
        self.cap.set(3, 320)
        self.cap.set(4, 240)
               # 加载训练好的卷积神经网络
        print("[INFO] loading network...")
        self.model = load_model(args["model"])
     # 预测函数，
    # 输入： 包含配置参数的字典
    
    def predict(self,args,frame):

    # 加载图像
        #image = cv2.imread(args["image"])
    # 因为对图像需要进行写入标签，影响较大所以复制一个图像
        orig = frame.copy()
 
    # 预处理图像进行分类
    # 图像的尺寸重载
        frame = cv2.resize(frame, (self.norm_size, self.norm_size))
    # 图像的序列的归一化处理
        frame = frame.astype("float") / 255.0
    # 将图像进行序列化
        frame = img_to_array(frame)
    # 展开数组的形状.
    # 插入一个新的轴，该轴将出现在扩展阵列形状的轴位置
        frame = np.expand_dims(frame, axis=0)
 
    # 对输入的图像进行分类
        result = self.model.predict(frame)[0]
    # print (result.shape)
        proba = np.max(result)
        label0 = str(np.where(result == proba)[0])
#         print(label0)
#         try:
#             motor_init()
#             while True:
#                 run(10)
#                 if label0 == '[0]':
#                     print("stop!")
#                     brake(5)
#                 elif label0 == '[1]':
#                     print("wait!")
#                     brake(3)       
#                 elif label0 == '[2]':
#                     print("stop!")
#                     brake(5)
#                 elif label0 == '[3]':
#                     print("stop!")
#                     brake(5)
#                 elif label0 == '[4]':
#                     print("straight!")
#                     run(5)
#                 elif label0 == '[5]':
#                     print("left!")
#                     spin_left(1)
#                     run(5)
#                 elif label0 == '[6]':
#                     print("right!")
#                     spin_right(1)
#                     run(5)
#         except KeyboardInterrupt:
#             pass
        label = "{}: {:.2f}%".format(label0, proba * 100)
#         print(label)
    
    # 在需要加载图像的情况下
#         if args['show']:
#             output = imutils.resize(orig, width=400)
#         # 在图像上绘制标签字符串
#             cv2.putText(output, label, (10, 25), cv2.FONT_HERSHEY_SIMPLEX,
#                         0.7, (0, 255, 0), 2)
#         # 显示带标签的图像
#             cv2.imshow("Output", output)
#             cv2.waitKey(0)     
        return label0


# python predict.py --model traffic_sign.model -i ../2.png -s
if __name__ == '__main__':
    args = {}
    # 模型的输入路径
    args['model'] = './MODE/traffic_sign.model'
    # 图像的输入路径
    #args['image'] = './predict/00022_00000.png'
    args['show'] = 'true' 
    try:
        motor_init() 
        SighRecong = SighRecongination()  
        # 执行预测
        print("sucess")
        while True:
            time.sleep(1)
            ret,frame =  SighRecong.cap.read()
            label0 = SighRecong.predict(args,frame)  
#             run(10)
            print(label0)

            if label0 == '[0]':
                print("stop!")
                brake(5)
            elif label0 == '[1]':
                print("wait!")
                brake(3)       
            elif label0 == '[2]':
                print("stop!")
                brake(5)
            elif label0 == '[3]':
                print("stop!")
                brake(5)
            elif label0 == '[4]':
                print("straight!")
                run(5)
            elif label0 == '[5]':
                print("left!")
                spin_left(1)
                run(5)
            elif label0 == '[6]':
                print("right!")
                spin_right(1)
                run(5)
    except KeyboardInterrupt:
        pass
            #print(label0)